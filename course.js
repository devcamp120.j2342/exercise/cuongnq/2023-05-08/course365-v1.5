const gBASE_URL = 'https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/';
const gCONTENT_TYPE = 'application/json;charset=UTF-8';

$(document).ready(function () {
	/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
	var gCourseId = 0;
	const gCOURSE_COLS = [
		'id',
		'courseCode',
		'courseName',
		'price',
		'duration',
		'level',
		'teacherName',
		'action',
	];
	var gCourseTable = $('#course-table').DataTable({
		columns: [
			{ data: gCOURSE_COLS[0] },
			{ data: gCOURSE_COLS[1] },
			{ data: gCOURSE_COLS[2] },
			{ data: gCOURSE_COLS[3] },
			{ data: gCOURSE_COLS[4] },
			{ data: gCOURSE_COLS[5] },
			{ data: gCOURSE_COLS[6] },
			{ data: gCOURSE_COLS[7] },
		],
		columnDefs: [
			{
				// định nghĩa lại cột action
				targets: 7,
				className: 'd-flex',
				defaultContent: `
      <button class = "btn btn-info edit-course" id="btn-sua">Sửa</button>
      <button class = "btn btn-danger ml-3 delete-course" id="btn-xoa">Xóa</button>
    `,
			},
		],
	});
	/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
	onPageLoading();
	$('#btn-add-course').on('click', function () {
		onBtnAddNewCourseClick();
	});

	// gán sự kiện cho nút Create course (trên modal)
	$('#btn-create-course').on('click', function () {
		onBtnCreateCourseClick();
	});

	// gán sự kiện cho nút update course (trên modal)
	$('#course-table').on('click', '.edit-course', function () {
		onBtnEditCourseClick(this);
	});

	$('#btn-update-course').on('click', function () {
		onBtnUpdateCourseClick();
	});

	$('#course-table').on('click', '.delete-course', function () {
		onBtnDeleteCourseClick(this);
	});

	$('#btn-confirm-delete-course').on('click', function () {
		onBtnDeleteCourseHandleClick();
	});
	/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
	function onPageLoading() {
		// 1 - R: Read / Load voucher to DataTable
		getAllCourse();
	}

	function onBtnAddNewCourseClick() {
		// hiển thị modal trắng lên
		$('#create-course-modal').modal('show');
	}

	function onBtnCreateCourseClick() {
		// khai báo đối tượng chứa voucher data
		var vCourseObj = {
			courseCode: '',
			courseName: '',
			price: '',
			discountPrice: '',
			duration: '',
			level: '',
			coverImage: '',
			teacherName: '',
			teacherPhoto: '',
			isPopular: '',
			isTrending: '',
		};
		// B1: Thu thập dữ liệu
		getCreateCourseData(vCourseObj);
		// B2: Validate insert
		var vIsCourseValidate = validateCourseData(vCourseObj);
		if (vIsCourseValidate) {
			// B3: insert voucher
			$.ajax({
				url: gBASE_URL + '/courses',
				type: 'POST',
				contentType: gCONTENT_TYPE,
				data: JSON.stringify(vCourseObj),
				success: function (paramRes) {
					// B4: xử lý front-end
					handleInsertCourseSuccess(paramRes);
				},
				error: function (paramErr) {
					console.log(paramErr.status);
				},
			});
		}
	}

	function onBtnEditCourseClick(paramBtnEdit) {
		// lưu thông tin voucherId đang được edit vào biến toàn cục
		gCourseId = getCourseIdFromButton(paramBtnEdit);
		// load dữ liệu vào các trường dữ liệu trong modal
		getCourseById(gCourseId);
	}

	function onBtnUpdateCourseClick() {
		// khai báo đối tượng chứa voucher data
		var vCourseObj = {
			courseCode: '',
			courseName: '',
			price: '',
			discountPrice: '',
			duration: '',
			level: '',
			coverImage: '',
			teacherName: '',
			teacherPhoto: '',
			isPopular: '',
			isTrending: '',
		};
		// B1: Thu thập dữ liệu
		getUpdateCourseData(vCourseObj);
		// B2: Validate insert
		var vIsCourseValidate = validateCourseData(vCourseObj);
		if (vIsCourseValidate) {
			// B3: insert voucher
			$.ajax({
				url: gBASE_URL + '/courses/' + gCourseId,
				type: 'PUT',
				contentType: gCONTENT_TYPE,
				data: JSON.stringify(vCourseObj),
				success: function (paramRes) {
					// B4: xử lý front-end
					handleUpdateCourseSuccess();
				},
				error: function (paramErr) {
					console.log(paramErr.status);
				},
			});
		}
	}

	function onBtnDeleteCourseClick(paramBtnEdit) {
		// lưu thông tin voucherId đang được edit vào biến toàn cục
		gCourseId = getCourseIdFromButton(paramBtnEdit);
		$('#delete-confirm-modal').modal('show');
	}

	function onBtnDeleteCourseHandleClick() {
		// khai báo đối tượng chứa voucher data
		// B1: Thu thập dữ liệu
		// B2: Validate insert
		// B3: insert voucher
		$.ajax({
			url: gBASE_URL + '/courses/' + gCourseId,
			type: 'DELETE',
			contentType: gCONTENT_TYPE,
			success: function (paramRes) {
				// B4: xử lý front-end
				handleDeleteUserSuccess();
			},
			error: function (paramErr) {
				console.log(paramErr.status);
			},
		});
	}
	/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

	function getAllCourse() {
		$.ajax({
			url: gBASE_URL + '/courses',
			type: 'GET',
			success: function (paramVouchers) {
				loadDataToCourseTable(paramVouchers);
			},
			error: function (paramErr) {
				console.log(paramErr.status);
			},
		});
	}
	function loadDataToCourseTable(paramVoucherArr) {
		gCourseTable.clear();
		gCourseTable.rows.add(paramVoucherArr);
		gCourseTable.draw();
	}

	function getCreateCourseData(paramUserObj) {
		paramUserObj.courseCode = $('#input-create-courseCode').val().trim();
		paramUserObj.courseName = $('#input-create-courseName').val().trim();
		paramUserObj.price = $('#input-create-price').val().trim();
		paramUserObj.discountPrice = $('#input-create-discountPrice').val().trim();
		paramUserObj.duration = $('#input-create-duration').val().trim();
		paramUserObj.level = $('#sel-level').val();
		paramUserObj.coverImage = $('#input-create-coverImage').val().trim();
		paramUserObj.teacherName = $('#input-create-teacherName').val().trim();
		paramUserObj.teacherPhoto = $('#input-create-teacherPhoto').val().trim();
		paramUserObj.isPopular = $('#sel-isPopular').val();
		paramUserObj.isTrending = $('#sel-isTrending').val();
	}

	function getUpdateCourseData(paramUserObj) {
		paramUserObj.courseCode = $('#input-update-courseCode').val().trim();
		paramUserObj.courseName = $('#input-update-courseName').val().trim();
		paramUserObj.price = $('#input-update-price').val().trim();
		paramUserObj.discountPrice = $('#input-update-discountPrice').val().trim();
		paramUserObj.duration = $('#input-update-duration').val().trim();
		paramUserObj.level = $('#sel-update-level').val();
		paramUserObj.coverImage = $('#input-update-coverImage').val().trim();
		paramUserObj.teacherName = $('#input-update-teacherName').val().trim();
		paramUserObj.teacherPhoto = $('#input-update-teacherPhoto').val().trim();
		paramUserObj.isPopular = $('#sel-update-isPopular').val();
		paramUserObj.isTrending = $('#sel-update-isTrending').val();
	}

	function validateCourseData(paramUserObj) {
		if (paramUserObj.courseCode === '') {
			alert('courseCode cần nhập');
			return false;
		}
		if (paramUserObj.courseName === '') {
			alert('courseName cần nhập');
			return false;
		}
		if (paramUserObj.price === '') {
			alert('price cần nhập');
			return false;
		}
		if (paramUserObj.discountPrice === '') {
			alert('discountPrice cần nhập');
			return false;
		}
		if (paramUserObj.duration === '') {
			alert('duration cần nhập');
			return false;
		}
		if (paramUserObj.level === '') {
			alert('level cần chọn');
			return false;
		}
		if (paramUserObj.coverImage === '') {
			alert('coverImage cần nhập');
			return false;
		}
		if (paramUserObj.teacherName === '') {
			alert('teacherName cần nhập');
			return false;
		}
		if (paramUserObj.teacherPhoto === '') {
			alert('teacherPhoto cần nhập');
			return false;
		}
		if (paramUserObj.isPopular === '') {
			alert('isPopular cần chọn');
			return false;
		}
		if (paramUserObj.isTrending === '') {
			alert('isTrending cần chọn');
			return false;
		}

		return true;
	}

	function handleInsertCourseSuccess() {
		alert('Thêm course thành công!');
		getAllCourse();
		resertCreateCourseForm();
		$('#create-course-modal').modal('hide');
	}

	function resertCreateCourseForm() {
		$('#input-create-courseCode').val('');
		$('#input-create-courseName').val('');
		$('#input-create-price').val('');
		$('#input-create-discountPrice').val('');
		$('#input-create-duration').val('');
		$('#sel-level').val('');
		$('#input-create-coverImage').val('');
		$('#input-create-teacherName').val('');
		$('#input-create-teacherPhoto').val('');
		$('#sel-isPopular').val('');
		$('#sel-isTrending').val('');
	}

	function resertUpdateCourseForm() {
		$('#input-update-courseCode').val('');
		$('#input-update-courseName').val('');
		$('#input-update-price').val('');
		$('#input-update-discountPrice').val('');
		$('#input-update-duration').val('');
		$('#sel-update-level').val('');
		$('#input-update-coverImage').val('');
		$('#input-update-teacherName').val('');
		$('#input-update-teacherPhoto').val('');
		$('#sel-update-isPopular').val('');
		$('#sel-update-isTrending').val('');
	}

	function getCourseById(paramVoucherId) {
		$.ajax({
			url: gBASE_URL + '/courses/' + paramVoucherId,
			type: 'GET',

			success: function (paramCourse) {
				showCourseDataToModal(paramCourse);
			},
			error: function (paramErr) {
				console.log(paramErr.status);
			},
		});
		// hiển thị modal lên
		$('#update-course-modal').modal('show');
	}

	function showCourseDataToModal(paramCourse) {
		$('#input-update-courseCode').val(paramCourse.courseCode);
		$('#input-update-courseName').val(paramCourse.courseName);
		$('#input-update-price').val(paramCourse.price);
		$('#input-update-discountPrice').val(paramCourse.discountPrice);
		$('#input-update-duration').val(paramCourse.duration);
		$('#sel-update-level').val(paramCourse.level);
		$('#input-update-coverImage').val(paramCourse.coverImage);
		$('#input-update-teacherName').val(paramCourse.teacherName);
		$('#input-update-teacherPhoto').val(paramCourse.teacherPhoto);
		$('#sel-update-isPopular').val(paramCourse.isPopular);
		$('#sel-update-isTrending').val(paramCourse.isTrending);
		console.log(paramCourse.isTrending);
	}

	function handleUpdateCourseSuccess() {
		alert('Sửa course thành công!');
		getAllCourse();
		resertUpdateCourseForm();
		$('#update-course-modal').modal('hide');
	}

	function handleDeleteUserSuccess() {
		alert('xóa course thành công!');
		getAllCourse();
		$('#delete-confirm-modal').modal('hide');
	}

	function getCourseIdFromButton(paramButton) {
		var vTableRow = $(paramButton).parents('tr');
		var vCourseRowData = gCourseTable.row(vTableRow).data();
		return vCourseRowData.id;
	}
});
