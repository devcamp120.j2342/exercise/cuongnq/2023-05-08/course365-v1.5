var gCoursesDB = {
	description: 'This DB includes all courses in system',
	courses: [
		{
			id: 1,
			courseCode: 'FE_WEB_ANGULAR_101',
			courseName: 'How to easily create a website with Angular',
			price: 750,
			discountPrice: 600,
			duration: '3h 56m',
			level: 'Beginner',
			coverImage: 'images/courses/course-angular.jpg',
			teacherName: 'Morris Mccoy',
			teacherPhoto: 'images/teacher/morris_mccoy.jpg',
			isPopular: false,
			isTrending: true,
		},
		{
			id: 2,
			courseCode: 'BE_WEB_PYTHON_301',
			courseName: 'The Python Course: build web application',
			price: 1050,
			discountPrice: 900,
			duration: '4h 30m',
			level: 'Advanced',
			coverImage: 'images/courses/course-python.jpg',
			teacherName: 'Claire Robertson',
			teacherPhoto: 'images/teacher/claire_robertson.jpg',
			isPopular: false,
			isTrending: true,
		},
		{
			id: 5,
			courseCode: 'FE_WEB_GRAPHQL_104',
			courseName: 'GraphQL: introduction to graphQL for beginners',
			price: 850,
			discountPrice: 650,
			duration: '2h 15m',
			level: 'Intermediate',
			coverImage: 'images/courses/course-graphql.jpg',
			teacherName: 'Ted Hawkins',
			teacherPhoto: 'images/teacher/ted_hawkins.jpg',
			isPopular: true,
			isTrending: false,
		},
		{
			id: 6,
			courseCode: 'FE_WEB_JS_210',
			courseName: 'Getting Started with JavaScript',
			price: 550,
			discountPrice: 300,
			duration: '3h 34m',
			level: 'Beginner',
			coverImage: 'images/courses/course-javascript.jpg',
			teacherName: 'Ted Hawkins',
			teacherPhoto: 'images/teacher/ted_hawkins.jpg',
			isPopular: true,
			isTrending: true,
		},
		{
			id: 8,
			courseCode: 'FE_WEB_CSS_111',
			courseName: 'CSS: ultimate CSS course from beginner to advanced',
			price: 750,
			discountPrice: 600,
			duration: '3h 56m',
			level: 'Beginner',
			coverImage: 'images/courses/course-css.jpg',
			teacherName: 'Juanita Bell',
			teacherPhoto: 'images/teacher/juanita_bell.jpg',
			isPopular: true,
			isTrending: true,
		},
		{
			id: 9,
			courseCode: 'FE_WEB_WORDPRESS_111',
			courseName: 'Complete Wordpress themes & plugins',
			price: 1050,
			discountPrice: 900,
			duration: '4h 30m',
			level: 'Intermediate',
			coverImage: 'images/courses/course-wordpress.jpg',
			teacherName: 'Clevaio Simon',
			teacherPhoto: 'images/teacher/clevaio_simon.jpg',
			isPopular: true,
			isTrending: false,
		},
		{
			id: 10,
			courseCode: 'FE_UIUX_COURSE_211',
			courseName: 'Thinkful UX/UI Design Bootcamp',
			price: 950,
			discountPrice: 700,
			duration: '5h 30m',
			level: 'Advanced',
			coverImage: 'images/courses/course-uiux.jpg',
			teacherName: 'Juanita Bell',
			teacherPhoto: 'images/teacher/juanita_bell.jpg',
			isPopular: false,
			isTrending: false,
		},
		{
			id: 11,
			courseCode: 'FE_WEB_REACRJS_210',
			courseName: 'Front-End Web Development with ReactJs',
			price: 1100,
			discountPrice: 850,
			duration: '6h 20m',
			level: 'Advanced',
			coverImage: 'images/courses/course-reactjs.jpg',
			teacherName: 'Ted Hawkins',
			teacherPhoto: 'images/teacher/ted_hawkins.jpg',
			isPopular: true,
			isTrending: true,
		},
		{
			id: 12,
			courseCode: 'FE_WEB_BOOTSTRAP_101',
			courseName: 'Bootstrap 4 Crash Course | Website Build & Deploy',
			price: 750,
			discountPrice: 600,
			duration: '3h 15m',
			level: 'Intermediate',
			coverImage: 'images/courses/course-bootstrap.png',
			teacherName: 'Juanita Bell',
			teacherPhoto: 'images/teacher/juanita_bell.jpg',
			isPopular: true,
			isTrending: false,
		},
		{
			id: 14,
			courseCode: 'FE_WEB_RUBYONRAILS_310',
			courseName: 'The Complete Ruby on Rails Developer Course',
			price: 2050,
			discountPrice: 1450,
			duration: '8h 30m',
			level: 'Advanced',
			coverImage: 'images/courses/course-rubyonrails.png',
			teacherName: 'Claire Robertson',
			teacherPhoto: 'images/teacher/claire_robertson.jpg',
			isPopular: false,
			isTrending: true,
		},
	],
};
$(document).ready(function () {
	onPageLoading();
});

function onPageLoading() {
	var vPopularData = getPopularData();
	console.log(vPopularData);
	showDataPopularToHtml(vPopularData);
	var vTrendingData = getTrendingData();
	showDataTrendingToHtml(vTrendingData);
}

function getPopularData() {
	var vKetQua = 0;
	var vKetQuaArr = [];
	var vIndex = 0;

	while (vKetQua < 4 && vIndex < gCoursesDB.courses.length) {
		if (gCoursesDB.courses[vIndex].isPopular === true) {
			vKetQua++;
			vKetQuaArr.push(gCoursesDB.courses[vIndex]);
			vIndex++;
		} else {
			vIndex++;
		}
	}
	return vKetQuaArr;
}

function getTrendingData() {
	var vKetQua = 0;
	var vKetQuaArr = [];
	var vIndex = 0;

	while (vKetQua < 4 && vIndex < gCoursesDB.courses.length) {
		if (gCoursesDB.courses[vIndex].isTrending === true) {
			vKetQua++;
			vKetQuaArr.push(gCoursesDB.courses[vIndex]);
			vIndex++;
		} else {
			vIndex++;
		}
	}
	return vKetQuaArr;
}

function showDataPopularToHtml(paramData) {
	var vDiv = $('#popular')[0];
	console.log(vDiv);
	for (var bI = 0; bI < paramData.length; bI++) {
		var DivColSm3 = $('<div>', {
			class: 'col-sm-3',
		}).appendTo(vDiv);
		var vDivCard = $('<div>', {
			class: 'card',
			style: 'width: 16rem',
		}).appendTo(DivColSm3);
		$('<img>', {
			src: paramData[bI].coverImage,
			class: 'card-img-top',
		}).appendTo(vDivCard);
		var vDivCardBody = $('<div>', {
			class: 'card-body px-4',
		}).appendTo(vDivCard);
		$('<a>', {
			href: '#',
			class: 'text-primary',
			html: paramData[bI].courseName,
		}).appendTo(vDivCardBody);
		var vPCardText = $('<p>', {
			class: 'card-text',
		}).appendTo(vDivCardBody);
		$('<i>', {
			class: 'far fa-clock',
		}).appendTo(vPCardText);
		$('<span>', {
			html: '&nbsp;' + paramData[bI].duration + ' ' + paramData[bI].level,
		}).appendTo(vPCardText);
		$('<p>', {
			class: 'card-text',
			html: '$' + paramData[bI].discountPrice,
		}).appendTo(vDivCardBody);
		var vDivCardHeader = $('<div>', {
			class: 'card-header d-flex justify-content-between py-3 align-item',
		}).appendTo(vDivCard);
		var vDivImg = $('<div>', {
			class: 'image d-flex',
		}).appendTo(vDivCardHeader);
		$('<img>', {
			src: paramData[bI].teacherPhoto,
			class: 'img-avatar',
			alt: paramData[bI].teacherName,
		}).appendTo(vDivImg);
		$('<span>', {
			class: 'name-teacher',
			html: paramData[bI].teacherName,
		}).appendTo(vDivImg);
		var vSpan = $('<span>').appendTo(vDivCardHeader);
		$('<i>', {
			class: 'far fa-bookmark',
		}).appendTo(vSpan);
	}
}
function showDataTrendingToHtml(paramData) {
	var vDiv = $('#trending')[0];
	console.log(vDiv);
	for (var bI = 0; bI < paramData.length; bI++) {
		var DivColSm3 = $('<div>', {
			class: 'col-sm-3',
		}).appendTo(vDiv);
		var vDivCard = $('<div>', {
			class: 'card',
			style: 'width: 16rem',
		}).appendTo(DivColSm3);
		$('<img>', {
			src: paramData[bI].coverImage,
			class: 'card-img-top',
		}).appendTo(vDivCard);
		var vDivCardBody = $('<div>', {
			class: 'card-body px-4',
		}).appendTo(vDivCard);
		$('<a>', {
			href: '#',
			class: 'text-primary',
			html: paramData[bI].courseName,
		}).appendTo(vDivCardBody);
		var vPCardText = $('<p>', {
			class: 'card-text',
		}).appendTo(vDivCardBody);
		$('<i>', {
			class: 'far fa-clock',
		}).appendTo(vPCardText);
		$('<span>', {
			html: '&nbsp;' + paramData[bI].duration + ' ' + paramData[bI].level,
		}).appendTo(vPCardText);
		$('<p>', {
			class: 'card-text',
			html: '$' + paramData[bI].discountPrice,
		}).appendTo(vDivCardBody);
		var vDivCardHeader = $('<div>', {
			class: 'card-header d-flex justify-content-between py-3 align-item',
		}).appendTo(vDivCard);
		var vDivImg = $('<div>', {
			class: 'image d-flex',
		}).appendTo(vDivCardHeader);
		$('<img>', {
			src: paramData[bI].teacherPhoto,
			class: 'img-avatar',
			alt: paramData[bI].teacherName,
		}).appendTo(vDivImg);
		$('<span>', {
			class: 'name-teacher',
			html: paramData[bI].teacherName,
		}).appendTo(vDivImg);
		var vSpan = $('<span>').appendTo(vDivCardHeader);
		$('<i>', {
			class: 'far fa-bookmark',
		}).appendTo(vSpan);
	}
}
